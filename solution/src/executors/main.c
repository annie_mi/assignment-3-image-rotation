#include "../headers/bmp.h"
#include "../headers/image.h"
#include "../headers/rotate.h"

#include <stdint.h>
#include <stdio.h>

int read_handle(enum read_status status, FILE *input, struct image *img) {
    switch(status){
        case READ_INVALID_HEADER: 
            fprintf(stderr, "Reading error: invalid header\n");
            fclose(input);
            clear_image(img);
            return 1;
        case READ_INVALID_BYTES:
            fprintf(stderr, "Reading error: invalid bytes\n");
            fclose(input);
            clear_image(img);
            return 1;
        default:
            return 0;
    }
}

int write_handle(enum write_status status, FILE *input, FILE *output, struct image *img, struct image *r_img) {
    switch(status){
        case WRITE_INVALID_HEADER:
            fprintf(stderr, "Writing error: invalid header\n");
            fclose(input);
            clear_image(img);
            fclose(output);
            clear_image(r_img);
            return 1;
        case READ_INVALID_BYTES:
            fprintf(stderr, "Writing error: invalid bytes\n");
            fclose(input);
            clear_image(img);
            fclose(output);
            clear_image(r_img);
            return 1;
        default:
            return 0;
    }
}

int main(int argc, char** argv){

     if (argc != 3)
     {
         fprintf(stderr, "Incorrect number of arguments");
         return 1;
     }

    FILE* file_input = fopen(argv[1], "rb");
    FILE* file_output = fopen(argv[2], "wb");
    if(file_input==NULL){
        fprintf(stderr, "File not found\n");
        return 1;
    }

    struct image input_img = {0};
    
    enum read_status read_status = from_bmp(file_input, &input_img);
    if(read_handle(read_status, file_input, &input_img) != 0) return 1;
    struct image output_img = rotate(input_img);
    enum write_status write_status = to_bmp(file_output, &output_img);
    if(write_handle(write_status, file_input, file_output, &input_img, &output_img) != 0) return 1;

    fclose(file_input);
    fclose(file_output);
    clear_image(&input_img);
    clear_image(&output_img);

    return 0;
}
