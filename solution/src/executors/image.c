#include "../headers/image.h"
#include <malloc.h>
#include <stdint.h>



void allocate_img(struct image *img, uint64_t w, uint64_t h) {
    img->width = w;
    img->height = h;
    // uint8_t padding = (4-(w*3)%4)%4;
    img->data = malloc(w * h * sizeof(struct pixel));
}

void clear_image(struct image *img)
{
    free(img->data);
}

