#include "../headers/rotate.h"
#include "malloc.h"

struct image rotate(struct image img)
{
    uint64_t width = img.height;
    uint64_t height = img.width;

    struct image r_img;
    allocate_img(&r_img, width, height);
    for (uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j = 0; j < width; j++)
        {
            r_img.data[i * width + j] = img.data[(img.height - j - 1) * img.width + i];
        }
    }
    return r_img;
}
